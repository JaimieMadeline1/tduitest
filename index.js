import React from 'react';
import ReactDOM from 'react-dom';
import { Root } from './src/_enter/Root';
import { configureStore } from './src/store/configureStore';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css'; 

const { store, persistor } = configureStore();

ReactDOM.render(
    <Root store={store} persistor={persistor} />,
    document.getElementById('root')
);