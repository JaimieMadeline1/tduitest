import React, { Component } from "react";
// import { ClipLoader } from "react-spinners";

import homeStyles from "./Home.styl";
// import colors from "../../colors/colors.styl";

// Global Styles, imported only once, folder not under css modules, so styles can be used in any file
import globalStyles from "../../styles/globalStyles.styl";

// import GlobalSpinner from '../../components/Global/GlobalSpinner';
import Header from "../Header/Header";
import Footer from "../Footer/Footer";

import RightMenu from "../../routing/RightMenu";

class Home extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <React.Fragment>
        {/* <GlobalSpinner showSpinner={true} ></GlobalSpinner> */}
        <Header />
        <div className={homeStyles.mainStyle}>
          <RightMenu />
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}

export default Home;
