import React, { Component } from "react";

import styles from "./HomePage.styl";

class HomePage extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <React.Fragment>
        <div className={styles.container}>
          <div className="container">
            <div className={["row", styles.innerContainer].join(" ")}>
              <div className={["col-lg-9", styles.mainBodySection].join(" ")}>
                <div className={styles.bodyTopContainerSection}>
                  <p className={[styles.sectionHeader].join("")}>Tell Us About Yourself</p>
                  <p className={styles.bodyText}>
                    Thanks for choosing TD Insurance! In order to safely set up your new accounts and services, please
                    tell us about yourself.
                  </p>
                </div>
                <div>
                  <p className={styles.sectionHeader}>Personal Information</p>
                  <div className={styles.bodyInputsContainer}>
                    <div className={["container", styles.inputRowContainer].join(" ")}>
                      <div className={["row", styles.inputsContainer].join(" ")}>
                        <div className="col-lg-2">
                          <button className={[styles.dropdown, styles.buttonDropdown].join(' ')}>Title*</button>
                          <i className={["fa fa-chevron-down", styles.buttonDropdownIcon].join(" ")} />
                        </div>
                        <div className="col-lg-3">
                          <input className={styles.inputStyle} placeholder="First Name*" />
                        </div>
                        <div className="col-lg-3">
                          <input className={styles.inputStyle} placeholder="Middle Initial*" />
                        </div>
                        <div className="col-lg-4">
                          <input className={styles.inputStyle} placeholder="Last Name*" />
                        </div>
                      </div>
                      <div>
                        <div className={["row", styles.inputsContainer].join(" ")}>
                          <div className="col-lg-5 allignDivs">
                            <input className={styles.inputStyle} placeholder="Email address*" />
                            <div className={styles.inputIconQuestionMarkContainer}>
                              <div className={styles.inputIconQuestionMark}>?</div>
                            </div>
                          </div>
                          <div className="col-lg-3">
                            <input className={styles.inputStyle} placeholder="Date of Birth*" />
                          </div>
                          <div className="col-lg-4 allignDivs">
                            <input className={styles.inputStyle} placeholder="Social Insurance Number*" />
                            <div className={styles.inputIconQuestionMarkContainer}>
                              <div className={styles.inputIconQuestionMark}>?</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className={styles.buttonContainer}>
                  <button className={["btn", styles.buttonCancel].join(" ")}>Cancel</button>
                  <button className={["btn", styles.buttonSubmit].join(" ")}>Submit</button>
                </div>
              </div>
              <div className={["col-lg-3", styles.rightBodySection].join(" ")}>
                <p className={styles.bodyText}>You are applying for:</p>
                <p className={styles.sectionHeader}>TD&reg; Travel Medical Insurace</p>
                <button className={["btn", styles.buttonCoverageOption].join(" ")}>See More Coverage Options</button>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default HomePage;
