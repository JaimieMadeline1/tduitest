import React, { Component } from "react";
import styles from "./Header.styl";
import tdLogo from "../../assets/TDLogo.png";
// const appConstants = require('../../constants/appConstants');

class Header extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <React.Fragment>
        <div className={styles.container}>
          <div className="container">
            <div className={[, styles.innerContainer, styles.flexTemp].join(" ")}>
              <div className={[styles.pullLeft].join(" ")}>
                <div className={[styles.iconSmallWidth, styles.navIcon].join(" ")}>
                  <a>
                    <i className={["fa fa-bars"].join(" ")} />
                  </a>
                </div>
                <a>
                  <img src={tdLogo} alt="TD Logo" />
                </a>
                <div className={styles.title}>TD Insurance</div>
              </div>
              <div className={[styles.pullLeft, styles.flexTemp, styles.advancedMenu].join(" ")}>
                <nav className={styles.headerNav}>
                  <ul>
                    <li>
                      <a>Products</a>
                    </li>
                    <li>
                      <a>Claims</a>
                    </li>
                    <li>
                      <a>Contact Us</a>
                    </li>
                  </ul>
                </nav>
              </div>
              <div className={[styles.pullRight, styles.rightNavMenu, styles.advancedMenu].join(" ")}>
                <ul>
                  <li>
                    <a>
                      Ontario <i className={["fa fa-chevron-down", styles.rightDropDownIcon].join(" ")} />
                    </a>
                  </li>
                  <li>
                    <a>
                      EN <i className={["fa fa-chevron-down", styles.rightDropDownIcon].join(" ")} />
                    </a>
                  </li>
                  <li>
                    <a>
                      <i className={["fa fa-search"].join(" ")} />
                    </a>
                  </li>
                  <li>
                    <a>
                      <i className={["fa fa-lock", styles.lockIcon].join(" ")} />
                      Login
                    </a>
                  </li>
                </ul>
              </div>
              <div className={styles.showWhenSmallWidth}>
                <div className={[styles.iconSmallWidth].join(" ")}>
                  <a>
                    <i className={["fa fa-lock"].join(" ")} />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Header;
