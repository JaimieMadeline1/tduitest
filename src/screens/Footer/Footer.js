import React, { Component } from "react";
import styles from "./Footer.styl";
import tdChair from "../../assets/TDChair.png";

class Footer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <React.Fragment>
        <div className={styles.container}>
          <div className="container">
            <div className="row">
              <div className={["col-lg-3", styles.tdChairContainer, styles.tdChairContainerLeftSide].join(" ")}>
                <img src={tdChair} alt="TD Chair" />
              </div>
              <div className={["col-lg-6", styles.footerBody].join(" ")}>
                <div className={styles.flexTemp}>
                  <div className={styles.footerQuote}>Need to talk to us directly?</div>
                  <div className={[styles.contactUsContainer]}>
                    <p className={styles.contactUs}>Contact us</p>
                    <i className={["fa fa-chevron-right", styles.contactUsIcon].join(" ")} />
                  </div>
                </div>
                <div className={[styles.divider].join(" ")} />
                <div className={[styles.flexTemp, styles.footerLinksContainer].join(" ")}>
                  <a>Privacy and Security</a>
                  <a>Legal</a>
                  <a>Accessibility</a>
                  <a>About Us</a>
                  <a>Careers</a>
                </div>
                <div className={[styles.divider].join(" ")} />
                <div className={[styles.iconsGroupContainer, styles.flexTemp].join(" ")}>
                  {["fa-facebook-f", "fa-twitter", "fa-youtube", "fa-wifi"].map(iconName => {
                    return (
                      <div className={[styles.iconContainer].join(" ")}>
                        <i
                          className={[
                            "fa",
                            iconName,
                            styles.footerIcon,
                            iconName == "fa-wifi" ? styles.iconTurnRight : ""
                          ].join(" ")}
                        />
                      </div>
                    );
                  })}
                </div>
              </div>
              <div className="col-lg-3" />
              <div className={["col-lg-3", styles.tdChairContainer, styles.tdChairContainerBottom].join(" ")}>
                <img src={tdChair} alt="TD Chair" />
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Footer;
